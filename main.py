import os
from flask import Flask
from flask_restful import Api
from Routes import initialize_routes


app = Flask(__name__)
api = Api(app)
initialize_routes(api)
if __name__ == "__main__":
    app.run(debug=False, host='0.0.0.0', port=int(os.environ.get('PORT', 8080)))

