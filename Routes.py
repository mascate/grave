from Api.HandwritingRecognition import HandwritingRecognition


def initialize_routes(api):
    api.add_resource(HandwritingRecognition, '/handwriting')

