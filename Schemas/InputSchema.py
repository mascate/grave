from marshmallow import Schema, fields, validates, ValidationError


class InputSchema(Schema):
    info = fields.List(fields.Int(), required=True, allow_none=False)  # List(fields.Int, required=True)
    label = fields.String()

    @validates('info')
    def validate_length(self, value):
        if len(value) != 1024:
            raise ValidationError('Quantity must be equal to 1024.')
