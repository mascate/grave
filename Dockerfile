FROM python:3.7

# Update OS
RUN sed -i 's/# \(.*multiverse$\)/\1/g' /etc/apt/sources.list
RUN apt-get update
RUN apt-get -y upgrade

COPY ./requirements.txt /webapp/requirements.txt
RUN pip3 install uwsgi
RUN pip3 install -r /webapp/requirements.txt

ADD . /webapp

ENV HOME /webapp
WORKDIR /webapp

# Expose port 8000 for uwsgi
EXPOSE 8080

ENTRYPOINT ["uwsgi", "--http", "0.0.0.0:8080", "--module", "main:app", "--processes", "5", "--threads", "2"]
