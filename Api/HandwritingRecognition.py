from typing import List, Dict
from flask_restful import Resource
from MachineLearning.KNN import KNN
from Schemas.InputSchema import InputSchema
from flask import Response, request


class HandwritingRecognition(Resource):

    __schema = InputSchema()
    __kNN = KNN()
    __errors: Dict[str, List[str]]

    def put(self):
        self.__errors = self.__schema.validate(request.json)
        if self.__errors:
            return self.__errors, 400

        self.__kNN.insert(self.__schema.load(request.json))
        return {}, 201

    def post(self):
        self.__errors = self.__schema.validate(request.json)
        if self.__errors:
            return self.__errors, 400

        output = self.__kNN.classify(self.__schema.load(request.json))
        return output.__dict__, 200
