import operator
import numpy as np
from os import listdir
from Schemas.Output import Output


class KNN:

    def __init__(self):
        self.__trainingMatrix = np.zeros((0, 0))
        self.__loadDataSet()

    def classify(self, inputSchema):
        output = Output()
        output.result = str(self.__classify(np.array(inputSchema["info"]), self.__trainingMatrix, self.__hwLabels, 3))
        return output

    def insert(self, inputSchema):
        self.__hwLabels.append(inputSchema["label"])
        self.__trainingMatrix = np.append(self.__trainingMatrix, np.array([inputSchema["info"]]), axis=0)

    def __loadDataSet(self):
        self.__hwLabels = []
        trainingFileList = listdir('TrainingDigits')  # load the training set
        m = len(trainingFileList)
        self.__trainingMatrix = np.zeros((m, 1024))
        for i in range(m):
            fileNameStr = trainingFileList[i]
            fileStr = fileNameStr.split('.')[0]  # take off .txt
            classNumStr = str((fileStr.split('_')[0]))
            self.__hwLabels.append(classNumStr)
            self.__trainingMatrix[i, :] = self.__imageToVector('TrainingDigits/%s' % fileNameStr)

    @staticmethod
    def __imageToVector(filename):
        returnVector = np.zeros((1, 1024))
        fr = open(filename)
        for i in range(32):
            lineStr = fr.readline()
            for j in range(32):
                returnVector[0, 32 * i + j] = int(lineStr[j])
        return returnVector

    @staticmethod
    def __classify(inX, dataSet, labels, k):
        dataSetSize = dataSet.shape[0]
        diffMat = np.tile(inX, (dataSetSize, 1)) - dataSet
        sqDiffMat = diffMat ** 2
        sqDistances = sqDiffMat.sum(axis=1)
        distances = sqDistances ** 0.5
        sortedDistIndices = distances.argsort()
        classCount = {}
        for i in range(k):
            voteLabel = labels[sortedDistIndices[i]]
            classCount[voteLabel] = classCount.get(voteLabel, 0) + 1
        sortedClassCount = sorted(classCount.items(), key=operator.itemgetter(1), reverse=True)
        return sortedClassCount[0][0]
